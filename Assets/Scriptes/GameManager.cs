﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager Instance { get; private set; }
    #endregion

    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject, Coin> coinConteiner;
    public Dictionary<GameObject, BuffReceiver> buffReceiverContainer;
    public Dictionary<GameObject, ItemComponent> itemsContainer;
    [HideInInspector] public PlayerInventory inventory;
    public ItemBase itemDataBase;
    [SerializeField] private GameObject panelInventory;

    private void Awake()
    {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();
        coinConteiner = new Dictionary<GameObject, Coin>();
        buffReceiverContainer = new Dictionary<GameObject, BuffReceiver>();
        itemsContainer = new Dictionary<GameObject, ItemComponent>();
    }

    public void Inventory()
    {
        if (!panelInventory.gameObject.activeInHierarchy)
        {
            panelInventory.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            panelInventory.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }
}
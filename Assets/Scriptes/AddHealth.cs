﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddHealth : MonoBehaviour
{
    [SerializeField] private int giveHealth;
    public int GiveHealth 
    {
        get { return giveHealth; }
        set
        {
            if (value > 0)
                giveHealth = value;
        }
    }
    [SerializeField] private Animator animator;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
        {
            var health = GameManager.Instance.healthContainer[col.gameObject];
            health.SetHealth(giveHealth);
            animator.SetTrigger("Disappear");
        }
    }

    public void DestroyFirstAid()
    {
        Destroy(gameObject);
    }
}

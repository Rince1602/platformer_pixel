﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour
{
    [SerializeField] private int coinsCount;
    public int CoinsCount
    {
        get { return coinsCount; }
        set
        {
            if (value > 0)
                coinsCount = value;
        }
    }
    [SerializeField] private Text coinsText;
    private List<Item> items;
    public List<Item> Items
    {
        get { return items; }
    }
    public BuffReceiver buffReceiver;

    private void Start()
    {
        coinsText.text = coinsCount.ToString();
        items = new List<Item>();
        GameManager.Instance.inventory = this;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.coinConteiner.ContainsKey(col.gameObject))
        {
            coinsCount++;
            coinsText.text = coinsCount.ToString();
            var coin = GameManager.Instance.coinConteiner[col.gameObject];
            coin.StartDestroy();
        }

        if (GameManager.Instance.itemsContainer.ContainsKey(col.gameObject))
        {
            var itemComponent = GameManager.Instance.itemsContainer[col.gameObject];
            items.Add(itemComponent.Item);
            itemComponent.Destroy(col.gameObject);
        }
    }
}

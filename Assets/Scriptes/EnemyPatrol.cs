﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public GameObject leftBorder;
    public GameObject rightBorder;
    public Rigidbody2D rigetbody;
    public GroundDetection groundDetection;
    [SerializeField] private bool isRightDirecrion;
    [SerializeField] private CollisionDamage collisionDamage;
    public bool IsRightDirection
    {
        get { return isRightDirecrion; }
        set { isRightDirecrion = value; }
    }
    [SerializeField] private float speed;
    public float Speed
    {
        get { return speed; }
        set
        {
            if (value >= 0.5f)
                speed = value;
        }
    }
    public Animator animator;
    public SpriteRenderer spriteRenderer;

    private void Update()
    {

        if (groundDetection.IsGrounded)
        {
            if (transform.position.x > rightBorder.transform.position.x || collisionDamage.Direction < 0)
                isRightDirecrion = false;
            else if (transform.position.x < leftBorder.transform.position.x || collisionDamage.Direction > 0)
                isRightDirecrion = true;
            rigetbody.velocity = isRightDirecrion ? Vector2.right : Vector2.left;
            rigetbody.velocity *= speed;
        }
        if (rigetbody.velocity.x > 0)
            spriteRenderer.flipX = true;
        if (rigetbody.velocity.x < 0)
            spriteRenderer.flipX = false;

    }
}

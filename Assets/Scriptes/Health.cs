﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private Animator animator;
    public int health
    {
        get { return _health; }
        set
        {
            if (value <= 100)
                _health = value;
        }
    }

    private void Start()
    {
        GameManager.Instance.healthContainer.Add(gameObject, this);
    }

    public void TakeHit(int damage)
    {
        health -= damage;

        if (animator != null)
        {
            animator.SetTrigger("Damage");           
        }

        if (health <= 0)
            Destroy(gameObject);
    }

    public void SetHealth(int bonusHealth)
    {
        health += bonusHealth;

        if (health > 100)
            health = 100;       
    }
}

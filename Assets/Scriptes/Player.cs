﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed;
    public float Speed
    {
        get { return speed; }
        set
        {
            if (value >= 0.5f)
                speed = value;
        }
    }
    [SerializeField] private float force = 15;
    public float Force
    {
        get { return force; }
        set
        {
            if (value >= 2f)
                speed = value;
        }
    }
    [SerializeField] private float minimalHeight = -15;
    public float MinimalHeight
    {
        get { return minimalHeight; }
        set
        {
            if (value < -10)
                speed = value;
        }
    }
    [SerializeField] private bool isCheatMode;
    public bool IsCheatMode
    {
        get { return isCheatMode; }
        set { isCheatMode = value; }
    }
    [SerializeField] private Arrow arrow;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float cooldownTime;
    [SerializeField] private int arrowsCount = 3;
    [SerializeField] private Health health;
    [SerializeField] private BuffReceiver buffReceiver;
    public Health Health { get { return health; } }
    public Rigidbody2D rigidbody; 
    public GroundDetection groundDetection;
    private Vector3 direction;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    private bool isJumping;   
    private float shootForce = 7;
    private bool shootCooldown = false;
    private Arrow currentArrow;
    private List<Arrow> arrowsPool;
    private float bonusForce;
    private float bonusHealth;
    private float bonusDamage;
    private UICharacterController controller;

    private void Awake()
    {
        Instance = this;
    }

    #region Singleton
    public static Player Instance { get; set; }
    #endregion

    public void InitUIController(UICharacterController uiController)
    {
        controller = uiController;
        controller.Jump.onClick.AddListener(Jump);
        controller.Fire.onClick.AddListener(CheckShoot);
    }

    private void Start()
    {
        arrowsPool = new List<Arrow>();
        for (int i = 0; i < arrowsCount; i++)
        {
            var arrowsTemp = Instantiate(arrow, arrowSpawnPoint);
            arrowsPool.Add(arrowsTemp);
            arrowsTemp.gameObject.SetActive(false);
        }

        buffReceiver.OnBuffChanged += ApplyBuffs;
    }

    private void ApplyBuffs()
    {
        var forceBuff = buffReceiver.Buffs.Find(t => t.type == BuffType.Force);
        var damageBuff = buffReceiver.Buffs.Find(t => t.type == BuffType.Damage);
        var armorBuff = buffReceiver.Buffs.Find(t => t.type == BuffType.Armor);
        bonusForce = forceBuff == null ? 0 : forceBuff.additiveBonus;
        bonusHealth = armorBuff == null ? 0 : armorBuff.additiveBonus;
        health.SetHealth((int)bonusHealth);
        bonusDamage = damageBuff == null ? 0 : damageBuff.additiveBonus;
    }
    void FixedUpdate()
    {
        Move();
        animator.SetFloat("Speed", Mathf.Abs(direction.x));
        CheckFall();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
            Jump();
#endif
    }

    private void CheckFall()
    {
        if (transform.position.y < minimalHeight && isCheatMode)
        {
            rigidbody.velocity = new Vector2(0, 0);
            transform.position = new Vector3(0, 0, 0);
        }
        else if (transform.position.y < minimalHeight && !isCheatMode)
            Destroy(gameObject);
    }

    private void Jump()
    {
        if (groundDetection.IsGrounded)
        {
            rigidbody.AddForce(Vector2.up * (force + bonusForce), ForceMode2D.Impulse);
            animator.SetTrigger("StartJump");
            isJumping = true;
        }
    }

    private void Move()
    {
        animator.SetBool("isGrounded", groundDetection.IsGrounded);
        if (!isJumping && !groundDetection.IsGrounded)
        {
            animator.SetTrigger("StartFall");
        }
        isJumping = isJumping && !groundDetection.IsGrounded;
        direction = Vector3.zero;
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left;
        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right;
#endif
        if (controller.Left.IsPressed)
            direction = Vector3.left;
        if (controller.Right.IsPressed)
            direction = Vector3.right;
        direction *= speed;
        direction.y = rigidbody.velocity.y;
        rigidbody.velocity = direction;
        if (direction.x > 0)
            transform.localScale = Vector3.one;
        if (direction.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);
    }

    private void InitArrow()
    {
        currentArrow = GetArrowFromPool();
        currentArrow.SetImpulse(Vector2.right, 0, 0, this);
    }


    private void Shoot()
    {
        currentArrow.SetImpulse(Vector2.right, transform.localScale.x < 0 ? -force * shootForce : force * shootForce, (int)bonusDamage, this);
        StartCoroutine(ShootCooldown());
    }

    private void CheckShoot()
    {
        if (!shootCooldown)
        {
            animator.SetTrigger("Attack");
            shootCooldown = true;
        }
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowsCount > 0)
        {
            var arrowTemp = arrowsPool[0];
            arrowsPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        return Instantiate(arrow, arrowSpawnPoint.position, Quaternion.identity);

    }

    public void ReturnArrowToPool(Arrow arrowTemp)
    {
        if (!arrowsPool.Contains(arrowTemp))
            arrowsPool.Add(arrowTemp);

        arrowTemp.transform.parent = arrowSpawnPoint;
        arrowTemp.transform.position = arrowSpawnPoint.transform.position;
        arrowTemp.gameObject.SetActive(false);
    }

    private IEnumerator ShootCooldown()
    {
        yield return new WaitForSeconds(cooldownTime);
        shootCooldown = false;
        yield break;
    }
}

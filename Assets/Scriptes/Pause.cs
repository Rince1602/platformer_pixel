﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    [SerializeField] private Text sound;

    private void Start()
    {
        if (PlayerPrefs.HasKey("Sound_Settings"))
            sound.text = PlayerPrefs.GetString("Sound_Settings");
    }
    public void OnClickPause()
    {
        Time.timeScale = 0;
        gameObject.SetActive(true);
    }

    public void OnClickContinue()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }

    public void OnClickMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void OnClickSound()
    {
        if (sound.text == "Sound: ON")
            sound.text = "Sound: OFF";
        else
            sound.text = "Sound: ON";
        PlayerPrefs.SetString("Sound_Settings", sound.text);
    }
}
